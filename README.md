# ` Serey Witness Node`

![N|Solid](https://drive.google.com/uc?export=view&id=1RF69XYb1ZPQ3kOVjQ9ysElFRo9D6GQAc)

# On the Serey Mission
The Serey mission can therefore be summarized as:

“Rewarding self-expression and creativity.”

# On the Serey platform and its features!

  Serey is principally a fork of Steemit — another social media platform on the blockchain — and therefore essentially makes use of the Graphene technology behind Steemit and Bitshares. However, where Steemit is trying to conquer the world, Serey is entirely dedicated to the people of Cambodia. Serey believes that regional differences require different user interfaces, and specific functionalities that match the people’s cultural makeup and their level of sophistication with blockchain technology. The Serey team have therefore chosen to create a platform with:

1) A brand new layout
2) A market place section
3) A Khmer language option
4) An advertisement section
5) A simplification of the reward system


### Requirements

Serey node is very powerful and you can use your pc to be witness :

* A Desktop , Laptop or Server (This guide assumes you're using Ubuntu 16.04 LTS) ,You can use other Linux distributions, but there may be slight differences.
* CPU: Dual core @ 2GHz or higher per core
* Memory: 8GB (16GB to be future-proof)
* Bandwidth: 3 mbit/s
* Storage: 200GB+ drive
* Knowledge of Basic Linux CommandLine
* Have serey account

### Installation

Serey Node requires [Docker CE](https://docs.docker.com/install/linux/docker-ce/ubuntu/) to run.
# Using Docker Image
# Step 1 : Install Docker CE
```sh
$ sudo apt-get update
$ sudo apt-get install  apt-transport-https  ca-certificates \
curl software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
$ sudo apt-get update
$ sudo apt-get install docker-ce
$ which docker
```
![N|Solid](https://drive.google.com/uc?export=view&id=1KhNaDtgpqCTM406b7LtWaKxsJ0BlwSVA)
# Step 2 : Create witness_node_data_dir
witness_node_data_dir is the directory to store all the information of your witness configuration file and block data.
create your witness_node_data_dir in path /opt/ (It can be any path , but I like to you in this path)
```sh
$ cd /opt/
$ mkdir witness_node_data_dir
```
![N|Solid](https://drive.google.com/uc?export=view&id=1FXBP5LOfKCA-Yadr5wQTtDHMBCh6qpiS)
Grant access your dir to have full access to read and write a new block
```sh
$ sudo chmod 777 witness_node_data_dir
```
![N|Solid](https://drive.google.com/uc?export=view&id=15n4GmPe2EnXhBmzVSz1AGZ4nL2HzJzNG)

# Step 3 : Create config.ini file
use nano config.ini to create file and then just copy the code below and paste, ctrl + X to save it.
```sh
$ nano config.ini
```
Copy code below
```sh
# Endpoint for P2P node to listen on
p2p-endpoint = 0.0.0.0:2001

# Maxmimum number of incoming connections on P2P endpoint
# p2p-max-connections =

# P2P network parameters
# p2p-parameters = {"listen_endpoint":"0.0.0.0:2001","accept_incoming_connections":true,"wait_if_endpoint_is_busy":true,"private_key":"00000000000000000000000000000","desired_number_of_connections":20,"maximum_number_of_connections":200,"peer_connection_retry_timeout":30,"peer_inactivity_timeout":50,"peer_advertising_disabled":false,"maximum_number_of_blocks_to_handle_at_one_time":200,"maximum_number_of_sync_blocks_to_prefetch":2000,"maximum_blocks_per_peer_during_syncing":200,"active_ignored_request_timeout_microseconds":6000000}

# P2P nodes to connect to on startup (may specify multiple times)
seed-node = 195.201.116.231:2001
seed-node = 195.201.88.98:2001
seed-node = 195.201.116.242:2001

# Pairs of [BLOCK_NUM,BLOCK_ID] that should be enforced as checkpoints.
# checkpoint =

# Location of the shared memory file. Defaults to data_dir/blockchain
# shared-file-dir =

# Size of the shared memory file. Default: 54G
shared-file-size = 6G

# Endpoint for websocket RPC to listen on
rpc-endpoint = 0.0.0.0:8090

# Endpoint for TLS websocket RPC to listen on
# rpc-tls-endpoint =

# Endpoint to forward write API calls to for a read node
# read-forward-rpc =

# The TLS certificate file for this server
# server-pem =

# Password for this certificate
# server-pem-password =

# API user specification, may be specified multiple times
# api-user =

# Set an API to be publicly available, may be specified multiple times
#public-api = database_api login_api account_by_key_api network_broadcast_api follow_api
public-api = database_api login_api account_by_key_api network_broadcast_api tag_api follow_api market_history_api raw_block_api

# Plugin(s) to enable, may be specified multiple times
#enable-plugin = witness account_history account_by_key follow
enable-plugin = witness account_history account_by_key tags follow market_history raw_block

# Maximum age of head block when broadcasting tx via API
max-block-age = 200

# Flush shared memory file to disk this many blocks
flush = 100000

# Whether to print backtrace on SIGSEGV
backtrace = yes

# Defines a range of accounts to track as a json pair ["from","to"] [from,to] Can be specified multiple times
# track-account-range =

# Defines a list of operations which will be explicitly logged.
# history-whitelist-ops =

# Defines a list of operations which will be explicitly ignored.
# history-blacklist-ops =

# Disables automatic account history trimming
history-disable-pruning = 0

# Track account statistics by grouping orders into buckets of equal size measured in seconds specified as a JSON array of numbers
account-stats-bucket-size = [60,3600,21600,86400,604800,2592000]

# How far back in time to track history for each bucker size, measured in the number of buckets (default: 100)
account-stats-history-per-bucket = 100

# Which accounts to track the statistics of. Empty list tracks all accounts.
account-stats-tracked-accounts = []

# Track blockchain statistics by grouping orders into buckets of equal size measured in seconds specified as a JSON array of numbers
chain-stats-bucket-size = [60,3600,21600,86400,604800,2592000]

# How far back in time to track history for each bucket size, measured in the number of buckets (default: 100)
chain-stats-history-per-bucket = 100

# Database edits to apply on startup (may specify multiple times)
# edit-script =

# RPC endpoint of a trusted validating node (required)
# trusted-node =

# Set the maximum size of cached feed for an account
follow-max-feed-size = 500

# Block time (in epoch seconds) when to start calculating feeds
follow-start-feeds = 0

# Track market history by grouping orders into buckets of equal size measured in seconds specified as a JSON array of numbers
market-history-bucket-size = [15,60,300,3600,86400]

# How far back in time to track history for each bucket size, measured in the number of buckets (default: 5760)
market-history-buckets-per-size = 5760

# Defines a range of accounts to private messages to/from as a json pair ["from","to"] [from,to)
# pm-account-range =

# Enable block production, even if the chain is stale.
enable-stale-production = true

# Percent of witnesses (0-99) that must be participating in order to produce blocks
required-participation = false

# name of witness controlled by this node (e.g. initwitness )
# witness = 

# WIF PRIVATE KEY to be used by one or more witnesses or miners
# private-key =

# declare an appender named "stderr" that writes messages to the console
[log.console_appender.stderr]
stream=std_error

# route any messages logged to the default logger to the "stderr" logger we
# declared above, if they are info level are higher
[logger.default]
#level=debug
level=error
appenders=stderr

# route messages sent to the "p2p" logger to the p2p appender declared above
[logger.p2p]
#level=debug
level=error
appenders=stderr
```
![N|Solid](https://drive.google.com/uc?export=view&id=1x53tFcuCrgJ5uIKaynal5CPpzW2duXTQ)

`This config is created and customized to work with Serey network.`

# Step 4 : Get Docker Image from Serey Repository
Serey Developers have created a docker image which able for all people to use and be a Witness .
Run the docker command to create a witness_node_data_dir in docker contain files.
```sh
docker run --rm -it -p 8090:8090 -p 2001:2001 -v /opt/witness_node_data_dir:/witness_node_data_dir baabeetaa/serey:run /opt/serey/steemd -d /witness_node_data_dir
```
![N|Solid](https://drive.google.com/uc?export=view&id=1O20XdAEaitc5Bl4Wv8qPR9AhkrxT_ZPp)
![N|Solid](https://drive.google.com/uc?export=view&id=1557d2psGWCM9oFmMryM1eEagLjy7DIQk)
after it run for 4 or 5 seconds, press CTRL + C to quit. Now you will see 1 folder created called "P2P" , it means you have create a node file configuration and peer list.
View Node config file to copy private key.
```sh
cat p2p/node_config.json
--------------------------
{
  "listen_endpoint": "0.0.0.0:2001",
  "accept_incoming_connections": true,
  "wait_if_endpoint_is_busy": true,
  "private_key": "xxxxxxxxxxxxxxxxxxxxxxxxxxx52c5eb129",
  "desired_number_of_connections": 20,
  "maximum_number_of_connections": 200,
  "peer_connection_retry_timeout": 30,
  "peer_inactivity_timeout": 5,
  "peer_advertising_disabled": false,
  "maximum_number_of_blocks_to_handle_at_one_time": 200,
  "maximum_number_of_sync_blocks_to_prefetch": 2000,
  "maximum_blocks_per_peer_during_syncing": 200,
  "active_ignored_request_timeout_microseconds": 6000000
```
![N|Solid](https://drive.google.com/uc?export=view&id=1SN5iPiZw_SA4YBU0UNpoZiq38zT62XNr)

Copy the private key and replace in config.ini on p2p-parameters  and Ctrl + X to save it.
```sh
# p2p-parameters = {"listen_endpoint":"0.0.0.0:2001","accept_incoming_connections":true,"wait_if_endpoint_is_busy":true,"private_key":"b738481bd3518075536fffb3d0c2e8beb0452a53423296bcea5af52c5eb129"`,"desired_number_of_connections":20,"maximum_number_of_connections":200,"peer_connection_retry_timeout":30,"peer_inactivity_timeout":50,"peer_advertising_disabled":false,"maximum_number_of_blocks_to_handle_at_one_time":200,"maximum_number_of_sync_blocks_to_prefetch":2000,"maximum_blocks_per_peer_during_syncing":200,"active_ignored_request_timeout_microseconds":6000000}
```
run docker command again to run and sync full node from serey , it's going to take a while (take a coffee break) Come back until you see this.
```sh
3597415ms th_a       application.cpp:553           handle_block         ] Got 0 transactions on block 8363028 by miner4 -- latency: 415 ms
```
![N|Solid](https://drive.google.com/uc?export=view&id=1UAbhUyT2-eqc_7g6DCzA8iNiXdaHrgYD)
it means you have sync full node to machine already. Are you done ? Almost.
# Step 5 : Update your account to be a witness and tell the server you are ready.
after you fully sync from serey , you can see all witness creating a new block every 3 seconds, so our goal is to be a part of serey witness node.
Go to terminal again and run docker cli-wallet.
### what is CLI Wallet?
CLI Wallet is command line tool to excute wallet of serey.

So in serey Docker files, contain two important parts are Sereyd and cli-wallet.
After we run first docker command , it means docker already run sereyd to do mining and vote on serey network.

Now run this command to view current docker image
```sh
$sudo docker ps -la
-----------------------
CONTAINER ID    IMAGE   COMMAND  CREATED   STATUS    PORTS      NAMES
83c5f6f79c2a        baabeetaa/serey:run   "/opt/serey/steemd -…"   21 minutes ago      Up 21 minutes       0.0.0.0:2001->2001/tcp, 0.0.0.0:8090->8090/tcp   fervent_heyrovsky
```
Copy Container ID and run this command 
```sh
$sudo docker exec -it 83c5f6f79c2a bash
----------------------------------------
root@83c5f6f79c2a:/#
```
![N|Solid](https://drive.google.com/uc?export=view&id=1KM6GAnBo0JUNwN1DZDe3DI5ADNt8Jd9W)
this command means we can go into serey docker image to view the contains. Now it's time to run Cli wallet
the ip 195.201.116.231:8090 is the main initminer node to communicate for updating witness node.
```sh
/opt/serey/cli_wallet -s ws://195.201.116.231:8090
-----------------------------------------------------
Logging RPC to file: logs/rpc/rpc.log
Starting a new wallet
3253920ms th_a       main.cpp:154                  main                 ] wdata.ws_server: ws://195.201.116.231:8090
3254620ms th_a       main.cpp:159                  main                 ] wdata.ws_user:  wdata.ws_password:
3254866ms th_a       websocket_api.cpp:88          on_message           ] message: {"id":1,"result":true}
3255297ms th_a       websocket_api.cpp:88          on_message           ] message: {"id":2,"result":0}
3255552ms th_a       websocket_api.cpp:88          on_message           ] message: {"id":3,"result":3}
Please use the set_password method to initialize a new wallet before continuing
```
First you need to run set_password as a default command.
```sh
set_password "Your Password"
```
Run command unlock 
```sh
unlock "Your Password"
```
![N|Solid](https://i.imgur.com/MwUzAzD.png)

and then run command import_key to import your private key from Serey account.
`This is very important to run this command, otherwise nothing will work for the rest of this tutorial`
`P.S` : at this part, we assume you already have serey account, if you do not have, please go to serey.io to register.
```sh
import_key "Your private key"
```
![N|Solid](https://drive.google.com/uc?export=view&id=1QqdxTC5H8rZ5n4XcwGAYMFB5zYv8v2yP)
Now it's time to tell the network thats you are ready to be the witness, but wait how serey is going to know, because we dont tell the network from our configuration file yet.
Let's go back to our config.ini and we have to update on two tags 
#name of witness controlled by this node (e.g. initwitness )
witness = "Your witness name"
#WIF PRIVATE KEY to be used by one or more witnesses or miners
private-key = your private key
`Dont forget` to untag Witness and Private-key field
![N|Solid](https://drive.google.com/uc?export=view&id=1cTdr4rrOzfvO99x79HluDVArTqQZPNZ5)
We are good to go now , run first docker command again.
Back to cli-wallet
Run command update_witness
```sh
update_witness "witness name" "https://serey.io" "Public key" {} true
```
![N|Solid](https://drive.google.com/uc?export=view&id=1NRcES5bBC1W4kil3xYWJVYsi3CSmPHWj)
Run command get_witness to verify
```sh
get_witness "witness name"
```
![N|Solid](https://drive.google.com/uc?export=view&id=1YEFdLzzhoHlea3oJ3Loou686QerfgF8X)

Boom !!! Now go back and see from your docker logs, you will see your witness name create a new block from logs.

# Congratulations You have a become a Serey witness node !!!

### Prepared by Serey Developers
If you have any questions, you can reach the team through:
E-mail: contact@serey.io. 
Facebook: Serey Platform / Serey.io


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
